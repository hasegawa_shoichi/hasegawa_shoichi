package board.service;

import java.sql.Connection;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import board.beans.Post;
import board.dao.PostDao;

import static board.utils.DButil.*;
import static board.utils.CloseableUtil.*;

public class PostService {

	public void register(Post post){
		Connection connection = null;
		try{
			connection = getConnection();
			PostDao postDao = new PostDao();
			postDao.insert(connection, post);
			commit(connection);
		}catch(RuntimeException e){
			rollback(connection);
			throw e;
		}catch(Error e){
			rollback(connection);
			throw e;
		}finally{
			close(connection);
		}
	}

	public List<Post> getPosts(String start, String end, String category){
		Connection connection = null;
		try{
			connection = getConnection();
			PostDao Post = new PostDao();
			List<Post> ret = Post.getPosts(connection, start, end, category);

			commit(connection);

			return ret;
		}catch(RuntimeException e){
			rollback(connection);
			throw e;
		}catch(Error e){
			rollback(connection);
			throw e;
		}finally{
			close(connection);
		}
	}

	public void delete(HttpServletRequest request) {
		Connection connection = null;
		int postId = Integer.parseInt(request.getParameter("postId"));
		try{
			connection = getConnection();
			PostDao Post = new PostDao();
			Post.deletePost(connection, postId);

			commit(connection);

		}catch(RuntimeException e){
			rollback(connection);
			throw e;
		}catch(Error e){
			rollback(connection);
			throw e;
		}finally{
			close(connection);
		}
	}
}
