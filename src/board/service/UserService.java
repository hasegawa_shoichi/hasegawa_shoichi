package board.service;

import static board.utils.DButil.*;
import static board.utils.CloseableUtil.*;

import java.sql.Connection;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import board.beans.User;
import board.dao.UserDao;
import board.utils.CipherUtil;

public class UserService {

	public void register(User user){

		Connection connection = null;
		try{
			connection = getConnection();
			String encPassword = CipherUtil.encrypt(user.getPassword());
			user.setPassword(encPassword);

			UserDao userDao = new UserDao();
			userDao.insert(connection, user);

			commit(connection);
		}catch(RuntimeException e){
			rollback(connection);
			throw e;
		}catch(Error e){
			rollback(connection);
			throw e;
		}finally{
			close(connection);
		}
	}

	public List<User> getUsers(){
		Connection connection = null;
		try{
			connection = getConnection();
			UserDao userDao = new UserDao();
			List<User> ret = userDao.getUsers(connection);

			commit(connection);
			return ret;
		}catch(RuntimeException e){
			rollback(connection);
			throw e;
		}catch(Error e){
			rollback(connection);
			throw e;
		}finally{
			close(connection);
		}
	}

	public User getUser(int userId){
		Connection connection = null;
		try{
			connection = getConnection();
			UserDao userDao = new UserDao();
			User ret = userDao.getUser(connection, userId);

			commit(connection);

			return ret;
		}catch(RuntimeException e){
			rollback(connection);
			throw e;
		}catch(Error e){
			rollback(connection);
			throw e;
		}finally{
			close(connection);
		}
	}

	public User editUser(HttpServletRequest request) {
		HttpSession session = request.getSession();
		User editUser = new User();
		User originUser = (User) session.getAttribute("editUser");
		editUser.setId(originUser.getId());

		if(request.getParameter("editLoginId").isEmpty()){
			editUser.setLoginId(originUser.getLoginId());
		}else{
			editUser.setLoginId(request.getParameter("editLoginId"));
		}

		if(request.getParameter("editName").isEmpty()){
			editUser.setName(originUser.getName());
		}else{
			editUser.setName(request.getParameter("editName"));
		}

		if(request.getParameter("editPassword").isEmpty()){
			editUser.setPassword("");
		}else{
			editUser.setPassword(request.getParameter("editPassword"));
		}
		
		editUser.setBranchId(Integer.parseInt(request.getParameter("editBranchId")));
		editUser.setDepartmentId(Integer.parseInt(request.getParameter("editDepartmentId")));
		return editUser;
	}

	public void updateUser(User editUser) {
		Connection connection = null;
		try{
			connection = getConnection();
			if(!(editUser.getPassword().isEmpty())){
				String encPassword = CipherUtil.encrypt(editUser.getPassword());
				editUser.setPassword(encPassword);
			}

			UserDao userDao = new UserDao();
			userDao.update(connection, editUser);

			commit(connection);
		}catch(RuntimeException e){
			rollback(connection);
			throw e;
		}catch(Error e){
			rollback(connection);
			throw e;
		}finally{
			close(connection);
		}
	}

	public void deleteUser(int userId) {
		Connection connection = null;
		try{
			connection = getConnection();

			UserDao userDao = new UserDao();
			User user = getUser(userId);
			userDao.deleteUser(connection, user);

			commit(connection);
		}catch(RuntimeException e){
			rollback(connection);
			throw e;
		}catch(Error e){
			rollback(connection);
			throw e;
		}finally{
			close(connection);
		}

	}

	public void rebirthUser(int userId) {
		Connection connection = null;
		try{
			connection = getConnection();

			UserDao userDao = new UserDao();
			User user = getUser(userId);
			userDao.rebirthUser(connection, user);

			commit(connection);
		}catch(RuntimeException e){
			rollback(connection);
			throw e;
		}catch(Error e){
			rollback(connection);
			throw e;
		}finally{
			close(connection);
		}
	}
}
