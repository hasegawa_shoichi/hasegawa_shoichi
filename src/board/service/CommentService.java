package board.service;

import java.sql.Connection;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import board.beans.Comment;
import board.dao.CommentDao;

import static board.utils.DButil.*;
import static board.utils.CloseableUtil.*;

public class CommentService {

	public void register(Comment comment){
		Connection connection = null;
		try{
			connection = getConnection();
			CommentDao commentDao = new CommentDao();
			commentDao.insert(connection, comment);
			commit(connection);
		}catch(RuntimeException e){
			rollback(connection);
			throw e;
		}catch(Error e){
			rollback(connection);
			throw e;
		}finally{
			close(connection);
		}
	}

	public List<Comment> getComments(){
		Connection connection = null;
		try{
			connection = getConnection();
			CommentDao commentDao = new CommentDao();
			List<Comment> ret = commentDao.getComment(connection);
			commit(connection);
			return ret;
		}catch(RuntimeException e){
			rollback(connection);
			throw e;
		}catch(Error e){
			rollback(connection);
			throw e;
		}finally{
			close(connection);
		}
	}

	public void delete(HttpServletRequest request) {
		Connection connection = null;
		int commentId = Integer.parseInt(request.getParameter("commentId"));
		try{
			connection = getConnection();
			CommentDao com = new CommentDao();
			com.deletePost(connection, commentId);

			commit(connection);

		}catch(RuntimeException e){
			rollback(connection);
			throw e;
		}catch(Error e){
			rollback(connection);
			throw e;
		}finally{
			close(connection);
		}
	}
}
