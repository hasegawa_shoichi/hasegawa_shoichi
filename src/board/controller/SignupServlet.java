package board.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


import board.beans.User;
import board.service.UserService;


@WebServlet("/signup")
public class SignupServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		String[] did = {"総務部", "IT部", "支店長", "社員"};
		String[] bid = {"本店", "A支店", "B支店", "C支店"};
		session.setAttribute("did", did);
		session.setAttribute("bid", bid);
		request.getRequestDispatcher("./signup.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		List<String> errorMessages = new ArrayList<String>();
		User user = new User();
		HttpSession session = request.getSession();
		String name = request.getParameter("name");
		String loginId = request.getParameter("login_id");
		String password = request.getParameter("password");
		int BranchId = Integer.parseInt(request.getParameter("branch_id"));
		int DepartmentId = Integer.parseInt(request.getParameter("department_id"));
		String[] did = {"総務部", "IT部", "支店長", "社員"};
		String[] bid = {"本店", "A支店", "B支店", "C支店"};
		session.setAttribute("did", did);
		session.setAttribute("bid", bid);
		if(!(password.equals(request.getParameter("checkPassword")))){
			errorMessages.add("パスワードの値が一致しません");
		}
		isValidLoginId(loginId, errorMessages);
		isValidName(name, errorMessages);
		isValidPassword(password, errorMessages);
		isValidBranchDepartmentId(BranchId, DepartmentId, errorMessages);
		if(errorMessages.size() > 0){
			session.setAttribute("branchId", BranchId);
			session.setAttribute("departmentId", DepartmentId);
			request.setAttribute("errorMessages", errorMessages);
			request.setAttribute("name", name);
			request.setAttribute("loginId", loginId);
			request.getRequestDispatcher("./signup.jsp").forward(request, response);
			return;
		}

		user.setName(name);
		user.setLoginId(loginId);
		user.setPassword(password);
		user.setBranchId(BranchId);
		user.setDepartmentId(DepartmentId);
		user.setIsDeleted(Integer.parseInt(request.getParameter("is_deleted")));

		new UserService().register(user);
		response.sendRedirect("./");
	}

	private void isValidBranchDepartmentId(int branchId, int departmentId, List<String> errorMessages) {
		if(branchId != 1 && (departmentId == 1 || departmentId == 2)){
			errorMessages.add("指定の支店と部署の組み合わせは存在しません");
		}else if(branchId == 3 && departmentId ==1){
			errorMessages.add("指定の支店と部署の組み合わせは存在しません");
		}else if(branchId == 1 && departmentId ==3){
			errorMessages.add("指定の支店と部署の組み合わせは存在しません");
		}else if(departmentId == 1 && branchId ==3){
			errorMessages.add("指定の支店と部署の組み合わせは存在しません");
		}

	}

	public void isValidName(String name, List<String> errorMessages){
		if(name.isEmpty()){
			errorMessages.add("名前を入力してください");
		}else if(name.length() > 10 ){
			errorMessages.add("名前の長さが不正です");
		}
	}

	public void isValidLoginId(String loginId, List<String> errorMessages){
		List<User> users = new UserService().getUsers();
		if(loginId.isEmpty()){
			errorMessages.add("ログインIDを入力してください");
		}else if(loginId.length() <= 6 || loginId.length() > 20 ){
			errorMessages.add("ログインIDの長さが不正です");
		}
		if(!(loginId.matches("[0-9a-zA-Z]*"))){
			errorMessages.add("ログインIDは半角英数字で入力してください");
		}
		for(User user: users){
			if(loginId.equals(user.getLoginId())){
				errorMessages.add("ログインIDが重複しています");
			}
		}
	}

	public void isValidPassword(String password, List<String> errorMessages){
		if(password.isEmpty()){
			errorMessages.add("パスワードを入力してください");
		}else if(password.length() <= 6 || password.length() > 20 ){
			errorMessages.add("パスワードの長さが不正です");
		}
		if(!(password.matches("\\w*"))){
			errorMessages.add("パスワードは _ を含む半角英数字で入力してください");
		}
	}
}
