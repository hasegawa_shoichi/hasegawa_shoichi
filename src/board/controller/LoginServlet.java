package board.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import board.beans.User;
import board.service.LoginService;

@WebServlet("/login")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("login.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		List<String> errorMessages = new ArrayList<String>();
		HttpSession session = request.getSession();

		if(!(isValid(request, errorMessages))){
			request.setAttribute("errorMessages", errorMessages);
			request.getRequestDispatcher("./login.jsp").forward(request, response);

		}else{
			String loginId = request.getParameter("login_id");
			String password = request.getParameter("password");
			LoginService loginService = new LoginService();
			User user = loginService.login(loginId, password);

			if(user != null){
				session.setAttribute("loginUser", user);
				response.sendRedirect("./");
			}else{
				request.setAttribute("login_id", loginId);
				errorMessages.add("ログインに失敗しました");
				request.setAttribute("errorMessages", errorMessages);
				request.getRequestDispatcher("./login.jsp").forward(request, response);
			}
		}
	}

	public boolean isValid(HttpServletRequest request, List<String> errorMessages){
		String loginId = request.getParameter("login_id");
		String password = request.getParameter("password");

		if(loginId.isEmpty()){
			errorMessages.add("ログインIDを入力してください");
		}else{
			request.setAttribute("login_id", loginId);
		}

		if(password.isEmpty()){
			errorMessages.add("パスワードを入力してください");
		}

		if(errorMessages.size() == 0){
			return true;
		}else{
			return false;
		}
	}
}
