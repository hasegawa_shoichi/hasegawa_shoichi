package board.controller;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.apache.commons.lang.StringUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import board.beans.Comment;
import board.beans.Post;
import board.beans.User;
import board.service.CommentService;
import board.service.PostService;

@WebServlet("/index.jsp")
public class MainServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		HttpSession session = request.getSession();

		if(request.getParameter("button") == null){
			cal.add(Calendar.DAY_OF_MONTH, 1);
			String today = sdf.format(cal.getTime());

			List<Post> post = new PostService().getPosts("2018-08-01", today , "null");
			List<Comment> comment = new CommentService().getComments();
			request.setAttribute("comments", comment);
			request.setAttribute("posts", post);
			request.getRequestDispatcher("./main.jsp").forward(request, response);
		}else{
			cal.add(Calendar.DAY_OF_MONTH, 1);
			String today = sdf.format(cal.getTime());
			String start = null;
			String end = null;
			String category = "null";

			if(!(request.getParameter("category").isEmpty())){
				category = (String) request.getParameter("category");
				request.setAttribute("category", category);
			}

			if(request.getParameter("start").isEmpty()){
				start = "2018-08-01";
			}else{
				start = request.getParameter("start");
				session.setAttribute("start", start);
			}

			if(request.getParameter("end").isEmpty()){
				end = today;
			}else{
				Date dend = null;
				try {
					session.setAttribute("end", (String) request.getParameter("end"));
					dend = sdf.parse((String) request.getParameter("end"));
				} catch (ParseException e) {
				}
				cal.setTime(dend);
				cal.add(Calendar.DAY_OF_MONTH, 1);
				end = sdf.format(cal.getTime());
			}
			List<Post> post = new PostService().getPosts(start, end , category);
			List<Comment> comment = new CommentService().getComments();


			session.setAttribute("comments", comment);
			request.setAttribute("posts", post);
			request.getRequestDispatcher("./main.jsp").forward(request, response);
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		List<String> errorMessages = new ArrayList<String>();
		if(request.getParameter("button").equals("postDelete")){

			new PostService().delete(request);

		}else if(request.getParameter("button").equals("commentDelete")){

			new CommentService().delete(request);

		}else if(request.getParameter("button").equals("postComment")){
			User user = (User) request.getSession().getAttribute("loginUser");
			Comment comment = new Comment();
			String text = request.getParameter("text");

			isValid(text, errorMessages);

			if(errorMessages.size() > 0){
				request.setAttribute("errorMessages", errorMessages);

			}else{
				comment.setUserId(user.getId());
				comment.setPostId(Integer.parseInt(request.getParameter("postId")));
				comment.setText(text);

				new CommentService().register(comment);
				}
			}
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.DAY_OF_MONTH, 1);
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			String today = sdf.format(cal.getTime());

			List<Post> post = new PostService().getPosts("2018-08-01", today , "null");
			List<Comment> comment = new CommentService().getComments();
			request.setAttribute("comments", comment);
			request.setAttribute("posts", post);
			request.getRequestDispatcher("./main.jsp").forward(request, response);

		}

	public void isValid(String text, List<String> errorMessages){
		if(text.length() > 500){
			errorMessages.add("コメントは500文字以内です");
		}
		if(StringUtils.isBlank(text)){
			errorMessages.add("空欄での投稿はできません");
		}
	}

}
