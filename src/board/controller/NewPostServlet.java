package board.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang.StringUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import board.beans.Post;
import board.beans.User;
import board.service.PostService;


@WebServlet("/newpost")
public class NewPostServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    	request.getRequestDispatcher("/newpost.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		List<String> errorMessages = new ArrayList<String>();

		HttpSession session = request.getSession();
		User user =(User) session.getAttribute("loginUser");
		String title = request.getParameter("title");
		String category = request.getParameter("category");
		String text = request.getParameter("text");
		
		isValidTitle(title, errorMessages);
		isValidCategory(category, errorMessages);
		isValidText(text, errorMessages);
		
		if(errorMessages.size() > 0){
			session.setAttribute("title", title);
			session.setAttribute("category", category);
			session.setAttribute("text", text);
			
			request.setAttribute("errorMessages", errorMessages);
			request.getRequestDispatcher("/newpost.jsp").forward(request, response);
			return;
		}

		Post post = new Post();
		post.setTitle(title);
		post.setCategory(category);
		post.setText(text);
		post.setUserId(user.getId());

		new PostService().register(post);

		response.sendRedirect("./");

	}
	public void isValidTitle(String title, List<String> errorMessages){
		if(StringUtils.isBlank(title)|| title.length() > 30 ){
			errorMessages.add("件名の長さが不正です");
		}
	}

	public void isValidText(String text, List<String> errorMessages){
		if(StringUtils.isBlank(text) || text.length() > 1000 ){
			errorMessages.add("本文の長さが不正です");
		}
	}

	public void isValidCategory(String category, List<String> errorMessages){
		if(StringUtils.isBlank(category) || category.length() > 10 ){
			errorMessages.add("カテゴリーの長さが不正です");
		}
	}
}
