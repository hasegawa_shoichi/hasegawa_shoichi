package board.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import board.beans.User;
import board.service.UserService;


@WebServlet("/usermanagement")
public class UserManagementServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		List<User> user = new UserService().getUsers();
		request.setAttribute("users", user);
		request.getRequestDispatcher("./usermanagement.jsp").forward(request, response);
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		if(request.getParameter("edit").equals("delete")){
			int userId = Integer.parseInt(request.getParameter("editUser"));
			new UserService().deleteUser(userId);

		}else if(request.getParameter("edit").equals("rebirth")){

			int userId = Integer.parseInt(request.getParameter("editUser"));
			new UserService().rebirthUser(userId);

		}
		List<User> user = new UserService().getUsers();
		request.setAttribute("users", user);
		request.getRequestDispatcher("./usermanagement.jsp").forward(request, response);
	}

}
