package board.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import board.beans.User;
import board.service.UserService;

@WebServlet("/edituser")
public class EditUserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		List<User> users = new UserService().getUsers();
		List<String> errorMessages = new ArrayList<String>();
		User user = null;
		HttpSession session = request.getSession();
		try{
			user = (User) new UserService().getUser(Integer.parseInt(request.getParameter("editUserId")));
			System.out.println("1");
			if(users.get(0).getId() > Integer.parseInt(request.getParameter("editUserId")) || users.get(users.size()-1).getId() < Integer.parseInt(request.getParameter("editUserId"))){
				throw new NumberFormatException();
			}
		}catch(NumberFormatException e){
			errorMessages.add("不正なパラメーターが送信されました");
			session.setAttribute("errorMessages", errorMessages);
			response.sendRedirect("usermanagement");
			return;
		}
		String[] did = {"総務部", "IT部", "支店長", "社員"};
		String[] bid = {"本店", "A支店", "B支店", "C支店"};
		session.setAttribute("did", did);
		session.setAttribute("bid", bid);
		session.setAttribute("editUser", user);
		session.setAttribute("branchId", user.getBranchId());
		session.setAttribute("departmentId", user.getDepartmentId());
		request.getRequestDispatcher("edituser.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		List<String> errorMessages = new ArrayList<String>();
		HttpSession session = request.getSession();
		String[] did = {"総務部", "IT部", "支店長", "社員"};
		String[] bid = {"本店", "A支店", "B支店", "C支店"};
		session.setAttribute("did", did);
		session.setAttribute("bid", bid);

		User editUser = (User) session.getAttribute("editUser");
		User loginUser = (User) session.getAttribute("loginUser");
		if(!(request.getParameter("editPassword").equals(request.getParameter("checkEditPassword")))){
			errorMessages.add("パスワードが一致していません");
		}

		if(!(request.getParameter("editLoginId").isEmpty())){
			isValidLoginId(request.getParameter("editLoginId"), editUser.getId(), errorMessages);
		}
		if(!(request.getParameter("editName").isEmpty())){
			isValidName(request.getParameter("editName"), errorMessages);
		}

		if(!(request.getParameter("editPassword").isEmpty())){
			isValidPassword(request.getParameter("editPassword"), errorMessages);
		}

		if(editUser.getId() != loginUser.getId()){
			isValidBranchDepartmentId(Integer.parseInt(request.getParameter("editBranchId")), Integer.parseInt(request.getParameter("editDepartmentId")), errorMessages);
		}

		if(errorMessages.size() > 0){
			session.setAttribute("branchId", Integer.parseInt(request.getParameter("editBranchId")));
			session.setAttribute("departmentId", Integer.parseInt(request.getParameter("editDepartmentId")));
			request.setAttribute("errorMessages", errorMessages);
			request.getRequestDispatcher("./edituser.jsp").forward(request, response);
			return;
		}

		User updateUser = new UserService().editUser(request);
		new UserService().updateUser(updateUser);
		response.sendRedirect("./usermanagement");
		return;
	}



	public void isValidName(String name, List<String> errorMessages){
		if(name.length() > 10 ){
			errorMessages.add("名前の長さが不正です");
		}
	}

	public void isValidLoginId(String loginId,int originId, List<String> errorMessages){
		List<User> users = new UserService().getUsers();
		if(loginId.length() > 20 || loginId.length() <= 6){
			errorMessages.add("ログインIDの長さが不正です");
		}
		if(!(loginId.matches("[0-9a-zA-Z]*"))){
			errorMessages.add("ログインIDは半角英数字で入力してください");
		}
		for(User user: users){
			if(loginId.equals(user.getLoginId()) &&(originId != user.getId())){
				errorMessages.add("ログインIDが重複しています");
			}
		}
	}

	public void isValidPassword(String password, List<String> errorMessages){
		if(password.length() > 20 || password.length() <= 6){
			errorMessages.add("パスワードの長さが不正です");
		}
		if(!(password.matches("\\w*"))){
			errorMessages.add("パスワードは _ を含む半角英数字で入力してください");
		}
	}

	public void isValidBranchDepartmentId(int branchId, int departmentId, List<String> errorMessages) {
		if(branchId != 1 && (departmentId == 1 || departmentId == 2)){
			errorMessages.add("指定の支店と部署の組み合わせは存在しません");
		}
		else if(branchId == 1 && departmentId ==3){
			errorMessages.add("指定の支店と部署の組み合わせは存在しません");
		}
		else if(branchId == 1 && departmentId ==4){
			errorMessages.add("指定の支店と部署の組み合わせは存在しません");
		}
		else if(departmentId == 3 && branchId ==1){
			errorMessages.add("指定の支店と部署の組み合わせは存在しません");
		}
	}
}