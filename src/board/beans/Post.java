package board.beans;

import java.io.Serializable;
import java.util.Date;

public class Post implements Serializable {
	private static final long serialVersionUID = 1L;

	private int id;
	private String title;
	private String text;
	private String category;
	private int userId;
	private String userName;
	private Date createdDate;
	private Date updatedDate;

	public int getId(){
		return id;
	}
	public String getTitle(){
		return title;
	}
	public String getText(){
		return text;
	}
	public String getCategory(){
		return category;
	}
	public int getUserId(){
		return userId;
	}
	public String getUserName(){
		return userName;
	}
	public Date getCreatedDate(){
		return createdDate;
	}
	public Date getUpdatedDate(){
		return updatedDate;
	}

	public void setId(int id){
		this.id = id;
	}
	public void setTitle(String title){
		this.title = title;
	}
	public void setText(String text){
		this.text = text;
	}
	public void setCategory(String category){
		this.category = category;
	}
	public void setUserId(int userId){
		this.userId = userId;
	}
	public void setUserName(String userName){
		this.userName = userName;
	}
	public void setCreatedDate(Date createdDate){
		this.createdDate = createdDate;
	}
	public void setUpdatedDate(Date updatedDate){
		this.updatedDate = updatedDate;
	}
}
