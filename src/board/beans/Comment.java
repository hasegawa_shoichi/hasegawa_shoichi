package board.beans;

import java.io.Serializable;
import java.util.Date;

public class Comment implements Serializable {
	private int id;
	private String text;
	private int userId;
	private String userName;
	private int postId;
	private Date createdDate;
	private Date updatedDate;

	public int getId(){
		return id;
	}
	public String getText(){
		return text;
	}
	public int getUserId(){
		return userId;
	}
	public String getUserName(){
		return userName;
	}
	public int getPostId(){
		return postId;
	}
	public Date getCreatedDate(){
		return createdDate;
	}
	public Date getUpdatedDate(){
		return updatedDate;
	}

	public void setId(int id){
		this.id = id;
	}
	public void setText(String text){
		this.text = text;
	}
	public void setUserId(int userId){
		this.userId = userId;
	}
	public void setUserName(String userName){
		this.userName = userName;
	}
	public void setPostId(int postId){
		this.postId = postId;
	}
	public void setCreatedDate(Date createdDate){
		this.createdDate = createdDate;
	}
	public void setUpdatedDate(Date updatedDate){
		this.updatedDate = updatedDate;
	}
}
