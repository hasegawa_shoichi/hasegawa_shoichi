package board.beans;

import java.io.Serializable;
import java.util.Date;

public class User implements Serializable {
	private static final long serialVersionUID = 1L;

	private int id;
	private String loginId;
	private String password;
	private String name;
	private int branchId;
	private String branchName;
	private int departmentId;
	private String departmentName;
	private int isDeleted;
	private Date createdDate;
	private Date updatedDate;

	public int getId(){
		return id;
	}
	public String getLoginId(){
		return loginId;
	}
	public String getPassword(){
		return password;
	}
	public String getName(){
		return name;
	}
	public int getBranchId(){
		return branchId;
	}
	public String getBranchName(){
		return branchName;
	}
	public int getDepartmentId(){
		return departmentId;
	}
	public String getDepartmentName(){
		return departmentName;
	}
	public int getIsDeleted(){
		return isDeleted;
	}
	public Date getCreatedDate(){
		return createdDate;
	}
	public Date getUpdatedDate(){
		return updatedDate;
	}

	public void setId(int id){
		this.id = id;
	}
	public void setLoginId(String loginId){
		this.loginId = loginId;
	}
	public void setPassword(String password){
		this.password = password;
	}
	public void setName(String name){
		this.name = name;
	}
	public void setBranchId(int branchId){
		this.branchId = branchId;
	}
	public void setBranchName(String branchName){
		this.branchName = branchName;
	}
	public void setDepartmentId(int departmentId){
		this.departmentId = departmentId;
	}
	public void setDepartmentName(String DepartmentName){
		this.departmentName = DepartmentName;
	}
	public void setIsDeleted(int isDeleted){
		this.isDeleted = isDeleted;
	}
	public void setCreatedDate(Date createdDate){
		this.createdDate = createdDate;
	}
	public void setUpdatedDate(Date updatedDate){
		this.updatedDate = updatedDate;
	}
}
