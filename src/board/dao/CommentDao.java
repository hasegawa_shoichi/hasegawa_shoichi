package board.dao;

import static board.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import board.beans.Comment;
import board.exception.SQLRuntimeException;



public class CommentDao {

	public void insert(Connection connection, Comment comment){
		PreparedStatement ps = null;
		try{
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO comments ( ");
			sql.append("text");
			sql.append(", user_id");
			sql.append(", post_id");
			sql.append(", created_date");
			sql.append(", updated_date");
			sql.append(") VALUES (");
			sql.append("?");
			sql.append(", ?");
			sql.append(", ?");
			sql.append(", CURRENT_TIMESTAMP");
			sql.append(", CURRENT_TIMESTAMP");
			sql.append(")");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, comment.getText());
			ps.setInt(2, comment.getUserId());
			ps.setInt(3, comment.getPostId());
			ps.executeUpdate();
		}catch(SQLException e){
			throw new SQLRuntimeException(e);
		}finally{
			close(ps);
		}
	}

	public List<Comment> getComment(Connection connection){
		PreparedStatement ps = null;
		try{
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
            sql.append("comments.id as id, ");
            sql.append("comments.text as text, ");
            sql.append("comments.post_id as post_id, ");
            sql.append("comments.user_id as user_id, ");
            sql.append("users.name as user_name, ");
            sql.append("comments.created_date as created_date ");
            sql.append("FROM comments ");
            sql.append("INNER JOIN users ");
            sql.append("ON comments.user_id = users.id ");
            sql.append("ORDER BY created_date DESC" );
			ps = connection.prepareStatement(sql.toString());
			ResultSet rs = ps.executeQuery();
			List<Comment> ret = toCommentList(rs);
			return ret;
		}catch(SQLException e){
			throw new SQLRuntimeException(e);
		}finally{
			close(ps);
		}
	}

	public List<Comment> toCommentList(ResultSet rs) throws SQLException{
		List<Comment> ret = new ArrayList<Comment>();
		try{
			while(rs.next()){
				Comment comment = new Comment();
				comment.setId(rs.getInt("id"));
				comment.setText(rs.getString("text"));
				comment.setPostId(rs.getInt("post_id"));
				comment.setUserId(rs.getInt("user_id"));
				comment.setUserName(rs.getString("user_name"));
				Timestamp ts = rs.getTimestamp("created_date");
				comment.setCreatedDate(ts);
				ret.add(comment);
			}
			return ret;
		}finally{
			close(rs);
		}
	}

	public void deletePost(Connection connection, int commentId) {
		PreparedStatement ps = null;
		try{
			String sql ="DELETE FROM comments WHERE id = ?";
			ps = connection.prepareStatement(sql);
			ps.setInt(1, commentId);
			ps.executeUpdate();
		}catch(SQLException e){
			throw new SQLRuntimeException(e);
		}finally{
			close(ps);
		}
	}
}
