package board.dao;

import static board.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import board.beans.Post;
import board.exception.SQLRuntimeException;

public class PostDao {
	public void insert(Connection connection, Post post){

		PreparedStatement ps = null;
		try{
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO posts ( ");
			sql.append("title");
			sql.append(", text");
			sql.append(", category");
			sql.append(", user_id");
			sql.append(", created_date");
			sql.append(", updated_date");
			sql.append(") VALUES (");
			sql.append("?");
			sql.append(", ?");
			sql.append(", ?");
			sql.append(", ?");
			sql.append(", CURRENT_TIMESTAMP");
			sql.append(", CURRENT_TIMESTAMP");
			sql.append(")");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, post.getTitle());
			ps.setString(2, post.getText());
			ps.setString(3, post.getCategory());
			ps.setInt(4, post.getUserId());
			ps.executeUpdate();
		}catch(SQLException e){
			throw new SQLRuntimeException(e);
		}finally{
			close(ps);
		}
	}

	public List<Post> getPosts(Connection connection,String start, String end, String category){
		PreparedStatement ps = null;
		try{
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("users.id as userId, ");
            sql.append("posts.title as title, ");
            sql.append("posts.text as text, ");
            sql.append("users.name as name, ");
            sql.append("posts.id as postId, ");
            sql.append("posts.category as category, ");
            sql.append("posts.created_date as created_date ");
            sql.append("FROM posts ");
            sql.append("INNER JOIN users ");
            sql.append("ON posts.user_id = users.id ");
            sql.append("WHERE ");
            sql.append("posts.created_date >= ? ");
            sql.append("AND posts.created_date < ? ");
            if(!(category.equals("null"))){
            	sql.append("AND posts.category LIKE ? ");
            }
            sql.append("ORDER BY created_date DESC");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, start);
			ps.setString(2, end);
			if(!(category.equals("null"))){
				ps.setString(3, "%" + category + "%");
			}
			ResultSet rs = ps.executeQuery();
			List<Post> ret = toPostList(rs);
			return ret;
		}catch(SQLException e){
			throw new SQLRuntimeException(e);
		}finally{
			close(ps);
		}
	}


	public void deletePost(Connection connection, int postId) {
		PreparedStatement ps = null;
		try{
			String sql ="DELETE FROM posts WHERE id = ?";
			ps = connection.prepareStatement(sql);
			ps.setInt(1, postId);
			ps.executeUpdate();
		}catch(SQLException e){
			throw new SQLRuntimeException(e);
		}finally{
			close(ps);
		}
	}

	private List<Post> toPostList(ResultSet rs) throws SQLException{
		List<Post> ret = new ArrayList<Post>();
		try{
			while(rs.next()){
				Post post = new Post();
				post.setTitle(rs.getString("title"));
				post.setText(rs.getString("text"));
				post.setUserName(rs.getString("name"));
				post.setId(rs.getInt("postId"));
				post.setUserId(rs.getInt("userId"));
				post.setCategory(rs.getString("category"));
				post.setCreatedDate(rs.getTimestamp("created_date"));
				ret.add(post);
			}
			return ret;
		}finally{
			close(rs);
		}
	}
}
