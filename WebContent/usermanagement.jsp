<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@page isELIgnored="false" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ユーザー管理</title>
<link href="./css/style.css" rel="stylesheet" type="text/css">
</head>
<body>
	<h2>ユーザー管理</h2>
	<div class="topcontents">
		<div class="link"><a href="signup">ユーザー新規登録</a></div>
		<div class="link"><a href="./">戻る</a></div>
	</div>
	<c:if test="${not empty errorMessages}">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages }" var="message">
						<li><c:out value="${message }"/></li>
					</c:forEach>
					<c:remove var="errorMessages" scope="session"/>
				</ul>
			</div>
		</c:if>

	<table class="users">
		<tr><th>id</th><th>氏名</th><th>ログインID</th><th>所属支店</th><th>所属部署</th><th>アカウント状態</th><th>作成日</th><th>変更日</th><th></th><th></th></tr>
			<c:forEach items="${users}" var="user">
				<tr>
					<th><c:out value="${user.id}"/></th>
					<th><c:out value="${user.name}"/></th>
					<th><c:out value="${user.loginId}"/></th>
					<th><c:out value="${user.branchName}"/></th>
					<th><c:out value="${user.departmentName}"/></th>
					<th><c:out value="${user.isDeleted}"/></th>
					<th><c:out value="${user.createdDate}"/></th>
					<th><c:out value="${user.updatedDate}"/></th>
					<th>
						<c:if test="${loginUser.id != user.id }">
							<c:if test="${user.isDeleted == 0 }">
								<form action="usermanagement" method="post">
									<button type="submit" name="edit" value="delete" onclick="return confirm('このユーザーを停止します。よろしいですか？')">停止</button>
									<input type="hidden" name="editUser" value="${user.id }">
								</form>
							</c:if>
							<c:if test="${user.isDeleted == 1 }">
								<form action="usermanagement" method="post">
									<button type="submit" name="edit" value="rebirth" onclick="return confirm('このユーザーを復活します。よろしいですか？')">復活</button>
									<input type="hidden" name="editUser" value="${user.id }">
								</form>
							</c:if>
						</c:if>
					</th>
					<th>
						<a href="edituser?editUserId=${user.id}">編集</a>
					</th>
				</tr>
			</c:forEach>
	</table>

</body>
</html>