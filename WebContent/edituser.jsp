<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@page isELIgnored="false" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ユーザー編集</title>
<link href="./css/style.css" rel="stylesheet" type="text/css">
</head>
<body>
	<h2>ユーザー編集</h2>
	<div class="returnpage"><a href="usermanagement">戻る</a></div>
	<div class="main-contents">
	<c:if test="${not empty errorMessages}">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages }" var="message">
						<li><c:out value="${message }"/></li>
					</c:forEach>
					<c:remove var="errorMessages" scope="session"/>
				</ul>
			</div>
		</c:if>
	<form action="edituser" method="post"><br/>
		名前<br/>
			<input name="editName" value="${editUser.name }" id="name"/><br/><br/>
		ログインID<br/>
			<input name="editLoginId" value="${editUser.loginId}" id="login_id"/><br/>
			半角英数6文字以上20文字以下<br/><br/>
			<table class="password">
					<tr><th>パスワード</th><th>確認用</th></tr>
					<tr><th><input type="password" name="editPassword" value="" id="editPassword"/></th><th><input type="password" name="checkEditPassword" value="" id="checkEditPassword"/></th>
					<tr><th colspan="2">記号を含む半角文字6文字以上20文字以下</th></tr>
				</table><br/>
			<c:if test="${editUser.id != loginUser.id }">
				所属支店
				<select name="editBranchId" id="editBranchId" >
					<c:forEach items="${bid }" var="branch" varStatus="status">
						<c:if test="${status.count == branchId }">
							<option value="${status.count}" selected>${branch }</option>
						</c:if>
						<c:if test="${status.count != branchId }">
							<option value="${status.count }">${branch }</option>
						</c:if>
					</c:forEach>
				</select><br/><br/>
				所属部署
				<select name="editDepartmentId" id="editDepartmentId">
					<c:forEach items="${did }" var="department"  varStatus="status">
						<c:if test="${status.count == departmentId }" >
							<option value="${status.count }" selected>${department }</option>
						</c:if>
						<c:if test="${status.count != departmentId }">
							<option value="${status.count }">${department }</option>
						</c:if>
					</c:forEach>
				</select>
			</c:if>
			<c:if test="${editUser.id == loginUser.id }">
				<input type="hidden" name="editBranchId" value="1">
				<input type="hidden" name="editDepartmentId" value="1">
			</c:if>
		<input type="hidden" name="editId" value="${edituser.id }"/><br/><br/>
		<input type="submit" value="編集"/>
	</form>
	</div>
</body>
</html>