<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
<head>
<meta charset=UTF-8>
<title>○○社掲示板</title>
<link href="./css/style.css" rel="stylesheet" type="text/css">
</head>
<body>
	<h2>○○社掲示板</h2>
	<div class="loginUser">ログインユーザー:<c:out value="${loginUser.name}"/></div>
	<div class="topcontents">
		<div class="link"><a href="newpost">新規投稿</a></div>
		<c:if test="${(loginUser.branchId == 1) && (loginUser.departmentId == 1)}">
			<div class="link"><a href="usermanagement">ユーザー管理</a></div>
		</c:if>
		<div class="link"><a href="logout">ログアウト</a></div>
	</div>
		<c:if test="${not empty errorMessages}">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages }" var="message">
						<li><c:out value="${message }"/></li>
					</c:forEach>
					<c:remove var="errorMessages" scope="session"/>
				</ul>
			</div>
		</c:if>

	<div class="narrowing">
		投稿絞込み
		<form action="./" method="get">
			期間:<input type="date" name="start" value="${start}"/><input type="date" name="end" value="${end }"/><br/>
			カテゴリー:<input name="category" value="${category }"><br/><br/>
			<button type="submit" name="button" value="narrowing">絞込み</button>
			<c:remove var="start" scope="session"/>
			<c:remove var="end" scope="session"/>
			<c:remove var="category" scope="session"/>
		</form>
	</div>
	<c:forEach items="${posts}" var="post">
	<div class="main-posts">
		<div class="comments">
		コメント<br/>
			<c:forEach items="${comments }" var="comment">
				<c:if test="${post.id == comment.postId }">
					<div class="comment">
						<div class="postuser">投稿者:<c:out value="${comment.userName }"/></div>
						<font size="2">本文</font><div class="commenttext"><pre><c:out value="${comment.text }"/></pre></div>
						<div class="commentdate">投稿日時<fmt:formatDate value="${comment.createdDate}" pattern="yyyy/MM/dd HH:mm:ss"/></div>
						<c:if test="${loginUser.id == comment.userId }">
						<div class="commentdelete">
							<form action="./" method="post">
								<button type="submit" name="button" value="commentDelete" onclick="return confirm('コメントを削除します。よろしいですか？')">コメント削除</button>
								<input type="hidden" name="commentId" value="${comment.id}"/>
							</form>
						</div>
						</c:if>
					</div>
				</c:if>
			</c:forEach>
		</div>
			<div class="subcontents">
				<div class="posts">
				<div class="posttitles"><font size="2">件名</font><br/>
					<h3><c:out value="${post.title}"/></h3></div>
				<font size="2">本文</font><div class="text" ><pre><c:out value="${post.text}"/></pre></div>
				<div class="username">投稿者:<c:out value="${post.userName}"/></div><br/>
				<table class="option" style="font-size: 10pt">
					<tr><th>カテゴリー:<c:out value="${post.category }"/></th></tr>
					<tr><th>投稿日時:<fmt:formatDate value="${post.createdDate}" pattern="yyyy/MM/dd HH:mm:ss"/></th></tr>
				</table>
				<div class="deletepost">
				<c:if test="${post.userId == loginUser.id }">
					<form action="./" method="post">
						<input type="hidden" name="postId" value="${post.id}"/>
						<button type="submit" name="button" value="postDelete" onclick="return confirm('投稿を削除しますよろしいですか？')">投稿削除</button>
					</form>
				</c:if>
				</div>
				</div>
				<div class="newcomment">
				<form action="./" method="post">
				<br/>
					<textarea name="text" rows="5" cols="100" class="tweet-box"></textarea><br/>500文字以下<br/>
					<input type="hidden" name="postId" value="${post.id}"/><br/>
					<button type="submit" name="button" value="postComment">コメント投稿</button>
				</form>
				</div>
			</div>
		</div>

	</c:forEach>
</body>
</html>