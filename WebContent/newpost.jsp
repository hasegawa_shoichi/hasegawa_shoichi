<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>新規投稿</title>
<link href="./css/style.css" rel="stylesheet" type="text/css">
</head>
<body>
	<h2>新規投稿</h2>
	<div class="returnpage"><a href="./">戻る</a></div>
	<c:if test="${not empty errorMessages}">
		<div class="errorMessages">
			<ul>
				<c:forEach items="${errorMessages }" var="message">
					<li><c:out value="${message }"/></li>
				</c:forEach>
				<c:remove var="errorMessages" scope="session"/>
			</ul>
		</div>
	</c:if>
	<div class="post-area">
		<form action="newpost" method="post">
			<div class="posttitle">件名<br/><input class="title" name="title" value="${title}" /><br/>30文字以下</div><br/>
			<div class="postcategory">カテゴリー<br/><input name="category" value="${category }" id="category"/><br/>10文字以下<br/></div><br/>
			本文<br/>
			<textarea name="text" rows="20" cols="50" class="texts">${text }</textarea><br/>
			1000文字以下<br/>
			<c:remove var="title" scope="session"/>
			<c:remove var="category" scope="session"/>
			<c:remove var="text" scope="session"/>
			<input type="submit" value="投稿">
		</form>
	</div>
</body>
</html>