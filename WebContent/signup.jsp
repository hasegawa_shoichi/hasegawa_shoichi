<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@page isELIgnored="false" %>
    <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>SignUp</title>
<link href="./css/style.css" rel="stylesheet" type="text/css">
</head>
<body>
	<h2>ユーザー登録</h2>
	<div class="returnpage"><a href="usermanagement">戻る</a></div>
	<div class="main-contents">
		<c:if test="${not empty errorMessages}">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages }" var="message">
						<li><c:out value="${message }"/></li>
					</c:forEach>
					<c:remove var="errorMessages" scope="session"/>
				</ul>
			</div>
		</c:if>
		<div class="loginform">
			<form action="signup" method="post">
				名前(必須)<br/>
					<input name="name" value="${name }" id="name"/><br/><br/>
				ログインID(必須)<br/>
					<input name="login_id" value="${login_id}" id="login_id"/><br/>
					半角英数6文字以上20文字以下<br/><br/>
				<table class="password">
					<tr><th>パスワード(必須)</th><th>確認用</th></tr>
					<tr><th><input type="password" name="password" value="" id="password"/></th><th><input type="password" name="checkPassword" value="" id="checkPassword"/></th>
					<tr><th colspan="2">記号を含む半角文字6文字以上20文字以下</th></tr>
				</table><br/>
				所属支店(必須)
				<select name="branch_id" id="branch_id">
					<c:forEach items="${bid }" var="branch" varStatus="status">
							<c:if test="${status.count == branchId }">
								<option value="${status.count}" selected>${branch }</option>
							</c:if>
							<c:if test="${status.count != branchId }">
								<option value="${status.count }">${branch }</option>
							</c:if>
					</c:forEach>
				</select><br/><br/>
				<label for="department_id">所属部署(必須)</label>
				<select name="department_id" id="department_id">
					<c:forEach items="${did }" var="department"  varStatus="status">
							<c:if test="${status.count == departmentId }" >
								<option value="${status.count }" selected>${department }</option>
							</c:if>
							<c:if test="${status.count != departmentId }">
								<option value="${status.count }">${department }</option>
							</c:if>
					</c:forEach>
				</select><br/><br/>
				<input type="hidden" name="is_deleted" value="0"/>
				<input type="submit" value="登録"/>
			</form>
		</div>
	</div>
</body>
</html>